import * as path from 'path';
import puppeteerCore, {
    BrowserConnectOptions,
    BrowserLaunchArgumentOptions,
    ElementHandle,
    HandleFor,
    LaunchOptions,
    Page,
    Product,
    PuppeteerNode,
    PuppeteerLifeCycleEvent
} from 'puppeteer-core';

export interface PrerenderOptions {
    debug?: boolean;
    launchOptions?: LaunchOptions &
        BrowserLaunchArgumentOptions &
        BrowserConnectOptions & {
            product?: Product;
            extraPrefsFirefox?: Record<string, unknown>;
        };
    port?: number | string;
    puppeteer?: PuppeteerNode | any;
    routes?: string[];
    setup?: (page: Page) => Promise<void>;
    waitFor?: number | string | (() => boolean | Promise<boolean>);
    waitForManual?: boolean;
    waitUntil?: PuppeteerLifeCycleEvent | PuppeteerLifeCycleEvent[];
}

export interface PrerenderOutput {
    html: string;
    route: string;
}

/**
 * Prerenders one or more pages based on provided routes using Puppeteer.
 * @param options PrerenderOptions
 * @returns A promise of the prerendered HTML string
 *
 * @example waitForTimeout `waitFor: 3000`
 *      This is time in milliseconds - rendering occurs once the duration has been reached.
 * @example waitForSelector `waitFor: '#app'`
 *      This will wait for any selector to exist on the DOM before rendering occurs.
 * @example waitForFunction `waitFor: () => !!window.APP_READY`
 *      To manually trigger:
 *      @usage `window.APP_READY = true;`
 * @example `waitForManual: true`
 *      When this is set, `waitFor` will be ignored. This expects a manual trigger of the prerender snapshot from the client code.
 *      This is great for waiting for async resolution & enrichment of content to the page.
 *      To manually Trigger:
 *      @usage `if (window.snapshot) window.snapshot();`
 */
export const prerender = async ({
    debug = false,
    launchOptions = {
        args: ['--no-sandbox'],
        headless: 'new'
    },
    port = process.env.PORT || 8080,
    puppeteer = puppeteerCore,
    routes = ['/'],
    setup,
    waitFor,
    waitForManual = false,
    waitUntil = 'domcontentloaded'
}: PrerenderOptions) => {
    const browser = await (puppeteer as PuppeteerNode).launch(launchOptions);
    const render = async (route = '/') => {
        const page = await browser.newPage();
        let output: PrerenderOutput;
        let watchDog:
            | Promise<HandleFor<unknown>>
            | Promise<ElementHandle<Element> | null>
            | null = null;

        console.info(`Prerender route ${route}...`);

        // Page Debugging
        if (debug) {
            // Captures browser events (console & network.)
            debugPage(page);
            console.info('Prerender launched w/ options:', launchOptions);
        }

        // Initialize the watchDog.
        if (waitForManual === true) {
            try {
                watchDog = page.waitForFunction(
                    () =>
                        new Promise<boolean>((resolve) => {
                            (window as any).__snapshot = () => {
                                resolve(true);
                            };
                        }),
                    {}
                );
            } catch (e) {
                console.error(
                    'PuppeteerWaitForFunctionError_Snapshot',
                    page.url(),
                    e
                );
            }
        } else if (waitFor) {
            if (typeof waitFor === 'string') {
                try {
                    watchDog = page.waitForSelector(waitFor);
                } catch (e) {
                    console.error('PuppeteerWaitForError', page.url(), e);
                }
            } else if (typeof waitFor === 'function') {
                try {
                    watchDog = page.waitForFunction(waitFor);
                } catch (e) {
                    console.error(
                        'PuppeteerWaitForFunctionError',
                        page.url(),
                        e
                    );
                }
            }
        }

        await page.evaluateOnNewDocument(() => {
            // Sets the global `__prerender` flag for the page's context.
            (window as any).__prerender = true;
        });

        if (setup) {
            // Page Setup
            await setup(page);
        }

        try {
            // Navigation (loads route)
            await page.goto(path.join(`http://localhost:${port}`, route), {
                waitUntil
            });
        } catch (e) {
            console.error('PuppeteerNavigationError', page.url(), e);
            output = { route, html: await page.content() };
            page.close();

            return output;
        }

        // Wait for the watchdog to resolve.
        if (watchDog !== undefined) {
            try {
                await watchDog;
            } catch (e) {
                console.error('PuppeteerWatchDogError', page.url(), e);
            }
        }

        // Serialized HTML
        output = { route, html: await page.content() };
        await page.close();
        console.info(`Closed tab at ${page.url()}`);

        return output;
    };
    const renderedPages = await Promise.all(routes.map(render));

    await browser.close();
    console.info('Browser session ended.');

    return renderedPages;
};

// Sets up the debug events for the given page.
const debugPage = (page: Page) =>
    page
        .on('console', (message) =>
            console.log(
                'PAGE_LOG',
                `${message
                    .type()
                    .substring(0, 3)
                    .toUpperCase()} ${message.text()}`
            )
        )
        .on('pageerror', ({ message }) => console.error('PAGE_ERR', message))
        .on('response', (response) =>
            console.log('NET_LOG', `${response.status()} ${response.url()}`)
        )
        .on('requestfailed', (request) =>
            console.log(
                `NET_ERR', ${request.failure()?.errorText} ${request.url()}`
            )
        );
