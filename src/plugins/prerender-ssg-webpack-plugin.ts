import * as path from 'path';
import { Compiler, WebpackError, WebpackPluginInstance } from 'webpack';
import {
    ssrServer,
    prerender,
    PrerenderOptions,
    PrerenderOutput
} from '../utils';

export type PrerenderSsgWebpackPluginOptions = PrerenderOptions;

export default class PrerenderSsgWebpackPlugin
    implements WebpackPluginInstance
{
    static readonly defaultOptions: PrerenderSsgWebpackPluginOptions = {
        routes: ['/']
    };
    private readonly fallbackAssetPath = '/';
    private options: PrerenderSsgWebpackPluginOptions;

    /**
     * Prerenders one or more pages based on provided routes using Puppeteer.
     * @param options PrerenderSsgWebpackPluginOptions
     *
     * @example waitForTimeout `waitFor: 3000`
     *      This is time in milliseconds - rendering occurs once the duration has been reached.
     * @example waitForSelector `waitFor: '#app'`
     *      This will wait for any selector to exist on the DOM before rendering occurs.
     * @example waitForFunction `waitFor: () => !!window.APP_READY`
     *      To manually trigger:
     *      @usage `window.APP_READY = true;`
     * @example `waitForManual: true`
     *      When this is set, `waitFor` will be ignored. This expects a manual trigger of the prerender snapshot from the client code.
     *      This is great for waiting for async resolution & enrichment of content to the page before rendering occurs.
     *      To manually Trigger:
     *      @usage `if (window.snapshot) window.snapshot();`
     */
    constructor(options: PrerenderSsgWebpackPluginOptions = {}) {
        this.options = {
            ...PrerenderSsgWebpackPlugin.defaultOptions,
            ...options
        };
    }

    // Define `apply` as its prototype method which is supplied with compiler as its argument
    apply(compiler: Compiler) {
        console.info('mode', compiler.options.mode);

        if (compiler.options.mode === 'development') {
            // Do nothing when in dev-mode.
            return;
        }

        // compiler.hooks.afterEmit.tapAsync(
        compiler.hooks.afterEmit.tapAsync(
            'PrerenderSsgWebpackPlugin',
            async (compilation, done) => {
                const {
                    output: { path: outputPath }
                } = compiler.options;
                const stats = compilation.getStats().toJson();
                const { port, routes, ...prerenderOptions } = this.options;
                let renderedPages: PrerenderOutput[] = [];
                // Start the server for prerendering.
                console.info('\nStarting the server...');
                const server = await ssrServer({
                    assetPath: stats.publicPath || this.fallbackAssetPath,
                    inputPath: outputPath,
                    port,
                    routes
                });
                console.info('Server started successfully.');

                try {
                    // Prerender HTML.
                    console.info('Starting prerender...');
                    renderedPages = await prerender({
                        ...prerenderOptions,
                        port,
                        routes
                    });
                    console.info('Prerender completed successfully.');
                } catch (e) {
                    handleError(e, 'Failed at prerender.');
                } finally {
                    // Stop the server.
                    server.close();
                }

                // The collection of emitted prerendered files.
                const emittedFiles: string[] = [];
                // Create the sorted set of rendered pages.
                const renderedPagesSet = new Set(
                    renderedPages.sort(
                        (a, b) => a.route.length - b.route.length
                    )
                );
                // Convert the Set to an iterator w/ next() function.
                const renderedPagesIterator =
                    renderedPagesSet[Symbol.iterator]();

                // Writes the files (filename based on route) synchronously for each rendered page.
                await new Promise<void>((resolve) => {
                    const writeFile = ({ content, route, staticFile }) =>
                        compiler.outputFileSystem.writeFile(
                            staticFile,
                            content,
                            (err) => {
                                if (err) {
                                    handleError(
                                        err,
                                        `Failed to write the static file, ${staticFile}.`
                                    );
                                } else {
                                    emittedFiles.push(`${route}/index.html`);
                                    console.info(
                                        `Emitted prerendered file "${(
                                            route + '/index.html'
                                        ).replace('//', '/')}"`
                                    );

                                    // Traverse to the next iteration.
                                    createStatic(renderedPagesIterator.next());
                                }
                            }
                        );
                    const createStatic = ({
                        done,
                        value
                    }: IteratorResult<
                        PrerenderOutput,
                        { html: string; route: string }
                    >) => {
                        if (done) {
                            // Resolve the promise once there are no more values to iterate over.
                            resolve();
                        } else {
                            const { html, route } = value;
                            const staticDir = path.join(
                                outputPath || '',
                                route
                            );
                            const staticFile = path.join(
                                staticDir,
                                'index.html'
                            );

                            if (route !== '/') {
                                compiler.outputFileSystem.mkdir(
                                    staticDir,
                                    (err) => {
                                        if (err && err.code !== 'EEXIST') {
                                            handleError(
                                                err,
                                                `Failed to make the ${route} directory.`
                                            );
                                        } else {
                                            writeFile({
                                                content: html,
                                                route,
                                                staticFile
                                            });
                                        }
                                    }
                                );
                            } else {
                                writeFile({
                                    content: html,
                                    route,
                                    staticFile
                                });
                            }
                        }
                    };

                    // Initialize the task of generating the HTML files.
                    createStatic(renderedPagesIterator.next());
                });

                console.info(
                    'Total prerendered files emitted',
                    emittedFiles.length
                );

                // End the compilation.
                done();

                function handleError(
                    err: Error,
                    failedMsg = 'Uh oh, something went wrong.'
                ) {
                    console.error(` | ${failedMsg}`, err);
                    compilation.errors.push(err as WebpackError);
                }
            }
        );
    }
}

// Fixes `Error: write EPIPE` & `Error: read ECONNRESET` for `stdout`.
process.stdout.on('error', (err) => {
    if (['EPIPE', 'ECONNRESET'].indexOf(err.code) >= 0) {
        process.stdout._write = function (_chunk, _enc, cb) {
            cb();
        };
        process.stdout._writev = function (_chunds, cb) {
            cb();
        };
        process.stdout._read = function () {
            this.push('');
        };

        return process.stdout.removeListener('error', onerror);
    }
});
