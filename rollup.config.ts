import terser from '@rollup/plugin-terser';
import typescriptRollupPlugin from '@rollup/plugin-typescript';
import { unlink } from 'fs';
import typescript from 'typescript';

import pkg from './package.json' assert { type: 'json' };

// Delete old typings to avoid issues
unlink('dist/index.d.ts', (_err) => {});

export default [
    // CommonJS (for Node) and ES module (for bundlers) build.
    // (We could have three entries in the configuration array
    // instead of two, but it's quicker to generate multiple
    // builds from a single configuration where possible, using
    // an array for the `output` option, where we can specify
    // `file` and `format` for each target)
    {
        input: 'src/index.ts',
        external: [...Object.keys(pkg.dependencies || {})],
        plugins: [
            // so Rollup can convert TypeScript to JavaScript
            typescriptRollupPlugin({
                sourceMap: true,
                tsconfig: './tsconfig.json',
                typescript: typescript
            }),
            terser()
        ],
        output: [
            { file: pkg.main, format: 'cjs', sourcemap: true },
            { file: pkg.module, format: 'es', sourcemap: true }
        ]
    }
];
