# Prerender SSG (Static Site Generation) Webpack Plugin

[![npm](https://img.shields.io/npm/v/prerender-ssg-webpack-plugin?color=green)](https://npmjs.com/package/prerender-ssg-webpack-plugin)
[![License: ISC](https://img.shields.io/badge/License-ISC-blue.svg)](https://opensource.org/licenses/ISC)

A Webpack plugin for prerendering configured routes after Webpack has emitted all assets from a build compilation.

> Requirements: Node v14+ and Webpack v5+

## About

After a successful production build, this plugin will:

-   spin up a Koa Server on the configured port (`output.path` by default),
-   create HTML content using Puppeteer's Headless Chromium Browser,
-   write HTML files to Webpack's `output.path` directory for each of the configured routes (`['/']` by default),
-   & replace all files inside Webpack's `output.path` directory for which already exist.

## Installation

`npm i -D prerender-ssg-webpack-plugin`

`yarn add -D prerender-ssg-webpack-plugin`

## Usage

```ts
import PrerenderSsgWebpackPlugin from 'prerender-ssg-webpack-plugin';

const webpackConfig = {
    plugins: [
        /**
         * This is the most basic example.
         * Defaults will be used.
         * See "Details, Options & Defaults" for information on configuring this plugin.
         */
        new PrerenderSsgWebpackPlugin()
    ]
};

module.exports = webpackConfig;
```

### Details, Options & Defaults (Optional)

```ts
/**
 * `PrerenderSsgWebpackPlugin`
 *
 * Prerenders one or more pages based on provided routes using Puppeteer.
 * This will only run for Webpack's production mode, otherwise it will abort.
 * @param options PrerenderSsgWebpackPluginOptions
 *
 * @example waitForSelector `waitFor: '#app'`
 *      This will wait for any selector to exist on the DOM before rendering occurs.
 * @example waitForFunction `waitFor: () => !!window.__APP_READY__`
 *      To manually trigger:
 *      @usage `window.__APP_READY__ = true;`
 * @example `waitForManual: true`
 *      When this is set, `waitFor` will be ignored. This expects a manual trigger of the prerender snapshot from the client code.
 *      This is great for waiting for async resolution & enrichment of content to the page before rendering occurs.
 *      To manually Trigger:
 *      @usage `if (window.__snapshot) window.__snapshot();`
 */

/**
 * `PrerenderSsgWebpackPluginOptions`
 *
 * These are the options which may be passed into `PrerenderSsgWebpackPlugin`.
 * All "options" are optional.
 * All defaults are set as values below, w/ allowable types in the comment following each option-key/value.
 */
const options: PrerenderSsgWebpackPluginOptions = {
    debug: false // boolean
    launchOptions: {
        args: ['--no-sandbox'],
        headless: true
    }, // puppeteer.LaunchOptions & puppeteer.BrowserLaunchArgumentOptions & puppeteer.BrowserConnectOptions & { product?: puppeteer.Product; extraPrefsFirefox?: Record<string, unknown>; }
    port: process.env.PORT || 8080, // number | string
    puppeteer: puppeteer, // PuppeteerNode
    routes: ['/'], // string[]
    setup: undefined, // (page: Page) => Promise<void>
    waitFor: undefined, // number | string | (() => boolean | Promise<boolean>)
    waitForManual: false, // boolean
    waitUntil: 'domcontentloaded' // 'domcontentloaded' | 'load' | 'networkidle0' | 'networkidle2'
};
```

### Example Webpack Config

```ts
import HtmlWebpackPlugin from 'html-webpack-plugin';
import * as path from 'path';
import PrerenderSsgWebpackPlugin from 'prerender-ssg-webpack-plugin';
import puppeteer from 'puppeteer';
import { Configuration, ProgressPlugin } from 'webpack';

const template = require('./templates/index.js');
const isProd = process.env.NODE_ENV === 'production';

const webpackConfig: Configuration = {
    entry: './src/index.ts',
    mode: isProd ? 'production' : 'development',
    module: {
        // TS & Styles' configs not shown for brevity.
        rules: [tsRuleSet, stylesRuleSet]
    },
    output: {
        clean: true,
        filename: '[name].[contenthash].bundle.js',
        path: path.resolve(process.cwd(), 'build'),
        publicPath: '/'
    },
    plugins: [
        new ProgressPlugin(),
        new HtmlWebpackPlugin({
            templateContent: template,
            title: 'Super Cool Title'
        }),
        new CopyPlugin({
            patterns: ['./assets/**/*']
        }),
        new PrerenderSsgWebpackPlugin({
            port: 3000,
            // Uses `puppeteer` from "puppeteer-core" by default.
            puppeteer,
            routes: ['/', '/about', '/contact', '/shop/products'],
            setup: async (page) => {
                await page.evaluateOnNewDocument(() => {
                    (window as any).SOME_FLAG = true;
                });
            },
            waitForManual: true
        })
    ]
};

module.exports = webpackConfig;
```

### Global Variables (in Browser Context)

-   `window.__prerender` will be set to `true` when this plugin is running.
-   `window.__snapshot` will be defined as a snapshot function only when the plugin option, `waitForManual`, is set to `true`.

### Deployments

For CI/CD build pipelines, you will likely need to avoid downloading chromium if using the "puppeteer" package instead of "puppeteer-core". In this case, set the following environment variable: `PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true`.

## ISC License (ISC)

Copyright (c) 2022 Claudio Nuñez Jr.

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
